﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

using Affdex;

namespace ConsoleApp1
{
    class ProcessImages : ImageListener, FaceListener
    {

        private Detector detector;
        
        public ProcessImages(Detector detector, Program program)
        {
            this.detector = detector;
            program.setProcessImages(this);
            this.detector.setImageListener(this);
            this.detector.setFaceListener(this);
            Console.WriteLine("Listener set on detector");
            fileName = TimeUtil.getDateString() + ".csv";
        }

        private Boolean firstLine = true;
        private String fileName;

        private DateTime lastProcessed = DateTime.Now;
        private Boolean confirmFaceSeen = false;

        public TimeSpan getTimeSinceLastProcessed()
        {
            return DateTime.Now.Subtract(lastProcessed);
        }

        public void resetLastProcessed()
        {
            lastProcessed = DateTime.Now;
        }

        void ImageListener.onImageResults(Dictionary<int, Face> faces, Frame frame)
        {
            //Console.WriteLine("onImageResults called");
            try { 
                if (faces == null || faces.Count ==0) 
                {
                    //Logger.log("null or zero face detected");
                }
                foreach (KeyValuePair<int, Face> pair in faces)
                {
                    if (!confirmFaceSeen)
                    {
                        Console.WriteLine("Face found: " + faces.Count);
                        Logger.log("Face found: " + faces.Count);
                        confirmFaceSeen = true;
                    }
                    lastProcessed = DateTime.Now;
                    Face face = pair.Value;
                    if (face != null)
                    {
                        StringBuilder builder = new StringBuilder();
                        StringBuilder flbuilder = null;
                        
                        long diff =TimeUtil.getUnixTime();
                        builder.Append(diff);
                        builder.Append(",");
                        if (firstLine)
                        {
                            flbuilder = new StringBuilder();
                            flbuilder.Append("Timestamp");
                            flbuilder.Append(",");
                        }
                        //System.Console.WriteLine("Part one done - now to the properties.");
                        foreach (PropertyInfo prop in typeof(Affdex.Emotions).GetProperties())
                        {
                            float value = (float)prop.GetValue(face.Emotions, null);
                            if (firstLine)
                            {
                                flbuilder.Append(prop.Name);
                                flbuilder.Append(",");                            
                            }
                            builder.Append(string.Format("{0:0.00}", value));
                            builder.Append(",");
                            //System.Console.WriteLine(output);
                        }

                        foreach (PropertyInfo eprop in typeof(Affdex.Expressions).GetProperties())
                        {
                            float evalue = (float)eprop.GetValue(face.Expressions, null);
                            if (firstLine)
                            {
                                flbuilder.Append(eprop.Name);
                                flbuilder.Append(",");
                            }
                            builder.Append(string.Format("{0:0.00}", evalue));
                            builder.Append(",");
                        }

                        //System.Console.WriteLine("Part two done - now to write.");                             
                        if (firstLine)
                        {
                            //System.Console.WriteLine(flbuilder.ToString());
                            flbuilder.Append("\n");
                            System.IO.File.AppendAllText(@"C:\Application Data\affectivdata\" + fileName, flbuilder.ToString());
                            firstLine = false;
                        }
                        //System.Console.WriteLine(builder.ToString());
                        builder.Append("\n");
                        System.IO.File.AppendAllText(@"C:\Application Data\affectivdata\" + fileName, builder.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error found in write file " + ex.Message);
            }
            finally
            {
                try { frame.Dispose(); } catch (Exception fex) { Console.WriteLine("finally fail " + fex.Message); }
            }
        }

        void ImageListener.onImageCapture(Frame frame)
        {
            //Console.WriteLine("onImageCapture called");
            //Logger.log("image capture");
            frame.Dispose();
        }
        
        // these are not the relevant calls
        public void	onFaceFound(float timestamp, int faceid) 
        {
            //Console.WriteLine("Face Listener started" + TimeUtil.getUnixTime());
            Logger.log("Face listener started");
        }

        public void	onFaceLost(float timestamp, int faceid)
        {
            //Console.WriteLine("Face Listener stopped" + TimeUtil.getUnixTime());
            Logger.log("Face listener stopped");
        }

    }


    static class TimeUtil
    {

        static DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long getUnixTime()
        {
            DateTime now = DateTime.Now;
            long diff = (long)(now.ToUniversalTime() - Jan1st1970).TotalMilliseconds;
            return diff;
        }

        public static String getDateString()
        {
            DateTime now = DateTime.Now;
            StringBuilder builder = new StringBuilder();
            builder.Append(Environment.MachineName);
            builder.Append(now.Year);
            if (now.Month<10)
            {
                builder.Append(0);
            }
            builder.Append(now.Month);
            if (now.Day < 10)
            {
                builder.Append(0);
            }
            builder.Append(now.Day);
            return builder.ToString();
        }

    }
}
