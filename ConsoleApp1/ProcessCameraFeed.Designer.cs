﻿using System.Collections.Generic;
using System.Windows.Forms;
using System.Reflection;
using Affdex;


namespace ConsoleApp1
{
    partial class ProcessCameraFeed : Form, Affdex.ImageListener
    {
        //Affdex.Detector detector;
        public ProcessCameraFeed(Affdex.Detector detector)
        {
            detector.setImageListener(this);
            InitializeComponent();
            //this.detector = detector;
        }




        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Text = "ProcessCameraFeed";
        }



        void ImageListener.onImageResults(Dictionary<int, Face> faces, Frame frame)
        {
            foreach(KeyValuePair<int, Face> pair in faces)
            {
                Face face = pair.Value;
                if (face != null)
                {
                    foreach(PropertyInfo prop in typeof(Affdex.Emotions).GetProperties())
                    {
                        float value = (float)prop.GetValue(face.Emotions, null);
                        string output = string.Format("{0}: {1:0.00}", prop.Name, value);
                        System.Console.WriteLine(output);
                    }
                }
            }
            frame.Dispose();
        }

        void ImageListener.onImageCapture(Frame frame)
        {
            frame.Dispose();
        }

        #endregion
    }
}