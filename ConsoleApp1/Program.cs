﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Reflection;
using System.Linq;
using System.Text;
using Affdex;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.IO;

namespace ConsoleApp1 
{

    class Program
    {
        private static Boolean run = true;

        private Affdex.Detector detector;
        public void setDetector(Affdex.Detector detector)
        {
            this.detector = detector;
        }
        static void Main(string[] args)
        {

            Console.CancelKeyPress += delegate (object sender, ConsoleCancelEventArgs e) {
                e.Cancel = true;
                run = false;
            };
            Program program = new Program();
            program.init();
        }

        public void init()
        { 
            Console.WriteLine("Start up: " + TimeUtil.getUnixTime());
            Logger.log("Start up");
            try
            {
                GetResponse();


            } catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            try { 
                // here
                Affdex.Detector createdDetector = new Affdex.CameraDetector(0, 30, 30, 1, Affdex.FaceDetectorMode.LARGE_FACES);
                setDetector(createdDetector);
                createProcessImages(detector);                
                while (run)
                {
                    System.Threading.Thread.Sleep(1000);
                    if (! detector.isRunning()) 
                    {
                        Console.WriteLine("Not running observerd: "  + TimeUtil.getUnixTime());
                        detector.start();
                    }
                    TimeSpan timeSinceLastProcessed = processImages.getTimeSinceLastProcessed();
                    Logger.log("Time since:" + timeSinceLastProcessed.Seconds.ToString() );
                    if (timeSinceLastProcessed.Minutes >= 1)
                    {
                        Logger.log("Over 1 minute found - reset detector");
                        //detector.reset();
                        detector.stop();
                        detector.Dispose();
                        Affdex.Detector newlyCreatedDetector = new Affdex.CameraDetector(0, 30, 30, 1, Affdex.FaceDetectorMode.LARGE_FACES);
                        setDetector(newlyCreatedDetector);
                        createProcessImages(detector);
                    }
                }
                Console.WriteLine("Stop Detector: " + TimeUtil.getUnixTime());
                Logger.log("Stop detector");
                detector.stop();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Terminate");
        }

        protected void createProcessImages(Detector detector)
        {
            ProcessImages processImages = new ProcessImages(detector, this);
            detector.setClassifierPath(".\\data");
            detector.setDetectAllEmotions(true);
            detector.setDetectAllExpressions(true);
            Console.WriteLine("Start detector: " + TimeUtil.getUnixTime());
            detector.start();
            Logger.log("Start detector");
            //return processImages;
        }
        
        ProcessImages processImages;

        public void setProcessImages(ProcessImages processImagesPass)
        {
            processImages = processImagesPass;
        }

        public static async void GetResponse()
        {
            var result = new StringBuilder();
            using (var client = new HttpClient())
            {
             
                StringBuilder sb = new StringBuilder();
                sb.Append("http://feele.exeter.ac.uk/timematch?machineName=");
                sb.Append(Environment.MachineName);
                sb.Append("&time=");             
                long diff = TimeUtil.getUnixTime();
                sb.Append((diff));
                Console.WriteLine(sb.ToString());
                client.BaseAddress = new Uri(sb.ToString());
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/text"));

                //string credentials = string.Format("{0}:{1}", username, password);
                //string encodedCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(credentials));
                //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", encodedCredentials);

                HttpResponseMessage response = await client.GetAsync("");
                if (response.IsSuccessStatusCode)
                {
                    Stream stream = await response.Content.ReadAsStreamAsync();
                    var sr = new StreamReader(stream);
                    string readLine = null;
                    while ((readLine = sr.ReadLine()) != null)
                    {
                        result.Append(readLine);
                    }
                }
            }
            //Console.WriteLine(result.ToString());
        }
    }

     static class Logger {
        private static object locko = new object();
        private static String logfiledir = @"C:\Application Data\affectivdata\";
        
        private static String logfile = logfiledir + TimeUtil.getDateString() + ".log";
        
        public static void log(String details) 
        {
            
            try {
                String message = TimeUtil.getUnixTime() + "\t" + details + "\r\n";
                lock(locko)
                {
                    System.IO.File.AppendAllText(logfile, message);
                }
            } catch (Exception ex) { Console.WriteLine(""+ ex.Message); }   

        }

        
    }


}
